//! # jpeg
//!
//! Module with useful JPEG constants and functions to parse JPEG format.

#![allow(dead_code)]

/// Temporary marker for arithmetic coding
pub const TEM: u8 = 0x01;

/// Start of frame (baseline JPEG)
pub const SOF0: u8 = 0xc0;

/// Start of frame (extended sequential, huffman)
pub const SOF1: u8 = 0xc1;

/// Start of frame (progressive, huffman)
pub const SOF2: u8 = 0xc2;

/// Start of frame (lossless, huffman)
pub const SOF3: u8 = 0xc3;

/// Define huffman tables
pub const DHT: u8 = 0xc4;

/// Start of frame (differential sequential, huffman)
pub const SOF5: u8 = 0xc5;

/// Start of frame (differential progressive, huffman)
pub const SOF6: u8 = 0xc6;

/// Start of frame (differential lossless, huffman)
pub const SOF7: u8 = 0xc7;

/// Reserved for JPEG extension
pub const JPG: u8 = 0xc8;

/// Start of frame (extended sequential, arithmetic)
pub const SOF9: u8 = 0xc9;

/// Start of frame (progressive, arithmetic)
pub const SOF10: u8 = 0xca;

/// Start of frame (lossless, arithmetic)
pub const SOF11: u8 = 0xcb;

/// Defined arithmetic coding conditioning
pub const DAC: u8 = 0xcc;

/// Start of frame (differential sequential, arithmetic)
pub const SOF13: u8 = 0xcd;

/// Start of frame (differential progressive, arithemtic)
pub const SOF14: u8 = 0xce;

/// Start of frame (differential lossless, arithmetic)
pub const SOF15: u8 = 0xcf;

/// Start of frame
pub const SOF47: u8 = 0xf7;

/// Restart marker 0
pub const RST0: u8 = 0xd0;

/// Restart marker 7
pub const RST7: u8 = 0xd7;

/// Start of image (parameterless)
pub const SOI: u8 = 0xd8;

/// End of image (parameterless)
pub const EOI: u8 = 0xd9;

/// Start of scan
pub const SOS: u8 = 0xda;

/// Define quantization table(s)
pub const DQT: u8 = 0xdb;

/// Define number of lines
pub const DNL: u8 = 0xdc;

/// Define restart interval
pub const DRI: u8 = 0xdd;

/// Define hierarchical progression
pub const DHP: u8 = 0xde;

/// Expand reference components
pub const EXP: u8 = 0xdf;

/// Extension data (comment)
pub const COM: u8 = 0xfe;

/// Application segment 0 (JFIF (len >=14) / JFXX (len >= 6) / AVI MJPEG)
pub const APP0: u8 = 0xe0;

/// Application segment 1 (EXIF/XMP/XAP)
pub const APP1: u8 = 0xe1;

/// Application segment 2 (FlashPix/ICC)
pub const APP2: u8 = 0xe2;

/// Application segment 3 (Kodak/...)
pub const APP3: u8 = 0xe3;

/// Application segment 4 (FlashPix/...)
pub const APP4: u8 = 0xe4;

/// Application segment 5 (Ricoh/...)
pub const APP5: u8 = 0xe5;

/// Application segment 6 (GoPro/...)
pub const APP6: u8 = 0xe6;

/// Application segment 7 (Pentax/Qualcomm)
pub const APP7: u8 = 0xe7;

/// Application segment 8 (Spiff)
pub const APP8: u8 = 0xe8;

/// Application segment 9 (MediaJukebox)
pub const APP9: u8 = 0xe9;

/// Application segment 10 (PhotoStudio)
pub const APP10: u8 = 0xea;

/// Application segment 11 (HDR)
pub const APP11: u8 = 0xeb;

/// Application segment 12 (photoshoP ducky / savE foR web)
pub const APP12: u8 = 0xec;

/// Application segment 13 (photoshoP savE As)
pub const APP13: u8 = 0xed;

/// Application segment 14 ("adobe" (length = 12))
pub const APP14: u8 = 0xee;

/// Application segment 15 (GraphicConverter)
pub const APP15: u8 = 0xef;

/// JPG0 extension data 00
pub const JPG0: u8 = 0xf0;

/// JPG6 extension data 06
pub const JPG6: u8 = 0xf6;

/// JPG9 extension data 09
pub const JPG9: u8 = 0xf9;

/// JPG13 extension data 00
pub const JPG13: u8 = 0xfd;

/// LSE extension parameters
pub const LSE: u8 = 0xf8;

/// Gets block name by given byte marker.
pub fn get_block_name(marker: u8) -> String {
    match marker {
        TEM => "temporary marker for arithmetic coding".to_string(),
        SOF0 => "start of frame (baseline JPEG)".to_string(),
        SOF1 => "start of frame (extended sequential, huffman)".to_string(),
        SOF2 => "start of frame (progressive, huffman)".to_string(),
        SOF3 => "start of frame (lossless, huffman)".to_string(),
        DHT => "define huffman tables".to_string(),
        SOF5 => "start of frame (differential sequential, huffman)".to_string(),
        SOF6 => "start of frame (differential progressive, huffman)".to_string(),
        SOF7 => "start of frame (differential lossless, huffman)".to_string(),
        JPG => "reserved for JPEG extension".to_string(),
        SOF9 => "start of frame (extended sequential, arithmetic)".to_string(),
        SOF10 => "start of frame (progressive, arithmetic)".to_string(),
        SOF11 => "start of frame (lossless, arithmetic)".to_string(),
        DAC => "defined arithmetic coding conditioning".to_string(),
        SOF13 => "start of frame (differential sequential, arithmetic)".to_string(),
        SOF14 => "start of frame (differential progressive, arithemtic)".to_string(),
        SOF15 => "start of frame (differential lossless, arithmetic)".to_string(),
        SOF47 => "start of frame (extension data section)".to_string(),
        RST0..=RST7 => format!("restart marker {}", marker - RST0),
        SOI => "start of image (parameterless)".to_string(),
        EOI => "end of image (parameterless)".to_string(),
        SOS => "start of scan".to_string(),
        DQT => "define quantization table(s)".to_string(),
        DNL => "define number of lines".to_string(),
        DRI => "define restart interval".to_string(),
        DHP => "define hierarchical progression".to_string(),
        EXP => "expand reference components".to_string(),
        COM => "extension data (comment)".to_string(),
        APP0..=APP15 => format!("application segment {}", marker - APP0),
        LSE => "LSE extension parameters".to_string(),
        JPG0..=JPG6 | JPG9..=JPG13 => {
            format!("JPG{} extension data {}", marker - JPG0, marker - JPG0)
        }
        _ => format!("unknown marker {:02x}", marker),
    }
}
