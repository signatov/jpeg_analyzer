//! # jpeg analyzer
//!
//! Simple JPEG image analysis application.
//!
//! Based on [reference](https://github.com/corkami/formats/blob/master/image/jpeg.md).

mod jpeg;

use ansi_term::Colour::{Green, Red};
use byteorder::{BigEndian, ByteOrder};
use clap::{App, Arg};
use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::process::exit;

/// Reads file into buffer.
///
/// # Examples
///
/// ```
/// let buffer = read_file("/etc/passwd");
/// assert!(!buffer.is_empty());
/// ```
fn read_file(fname: &str) -> io::Result<Vec<u8>> {
    let mut fd = File::open(&fname)?;
    let mut buffer = Vec::new();
    fd.read_to_end(&mut buffer)?;
    Ok(buffer)
}

/// Formats byte into red hex string.
fn red(code: u8) -> String {
    Red.paint(format!("{:02x}", code)).to_string()
}

/// Formats byte into green hex string.
fn green(code: u8) -> String {
    Green.paint(format!("{:02x}", code)).to_string()
}

/// Analyzes file buffer.
fn analyze_buffer(buffer: &[u8]) {
    let mut i = 0;
    while i < buffer.len() {
        let ff = buffer[i];
        let marker = buffer[i + 1];
        if ff != 0xff {
            println!("{}", Red.paint("ERROR: this file is not a JPEG image"));
            exit(1);
        }
        match marker {
            jpeg::SOI => {
                println!(
                    "{:08.X}: {} {} {}",
                    i,
                    red(ff),
                    green(marker),
                    jpeg::get_block_name(marker)
                );
                i += 2;
            }
            jpeg::EOI => {
                println!(
                    "{:08.X}: {} {} {}",
                    i,
                    red(ff),
                    green(marker),
                    jpeg::get_block_name(marker)
                );
                return;
            }
            0x00 | jpeg::SOS => {
                println!(
                    "{:08.X}: {} {} {}",
                    i,
                    red(ff),
                    green(marker),
                    jpeg::get_block_name(marker)
                );
                i += 2;
                while buffer[i] != 0xff {
                    i += 1;
                }
            }
            jpeg::SOF0..=jpeg::SOF3
            | jpeg::SOF5..=jpeg::SOF7
            | jpeg::SOF9..=jpeg::SOF11
            | jpeg::SOF13..=jpeg::SOF15 => {
                println!(
                    "{:08.X}: {} {} {}",
                    i,
                    red(ff),
                    green(marker),
                    jpeg::get_block_name(marker)
                );
                let length = BigEndian::read_u16(&buffer[i + 2..i + 4]);
                i += 2 + length as usize;
            }
            jpeg::RST0..=jpeg::RST7 => {
                let length = BigEndian::read_u16(&buffer[i + 2..i + 4]);
                println!(
                    "{:08.X}: {} {} {} {}",
                    i,
                    red(ff),
                    green(marker),
                    length,
                    jpeg::get_block_name(marker)
                );
                i += 2 + length as usize;
            }
            jpeg::APP0 => {
                let length = BigEndian::read_u16(&buffer[i + 2..i + 4]);
                let segment_type = if length >= 14 {
                    "JFIF"
                } else if length >= 6 {
                    "JFXX"
                } else {
                    "AVI MJPEG"
                };
                println!(
                    "{:08.X}: {} {} {} {}",
                    i,
                    red(ff),
                    green(marker),
                    jpeg::get_block_name(marker),
                    segment_type
                );
                i += 2 + length as usize;
            }
            _ => {
                let length = BigEndian::read_u16(&buffer[i + 2..i + 4]);
                println!(
                    "{:08.X}: {} {} {} len={}",
                    i,
                    red(ff),
                    green(marker),
                    jpeg::get_block_name(marker),
                    length
                );
                i += 2 + length as usize;
            }
        }
    }
}

/// Analyzes given file.
fn analyze(fname: &str) {
    println!("Analyze {}", fname);
    match read_file(&fname) {
        Ok(v) => analyze_buffer(&v),
        Err(e) => println!("{}", Red.paint(format!("Error: {}", e.to_string()))),
    }
}

/// Program entry point.
fn main() {
    let matches = App::new("JPEG tiny analyzer")
        .version("0.0.1")
        .author("Sergey Ignatov")
        .about("Analyzes input JPEG file and shows file format data")
        .arg(
            Arg::with_name("file")
                .short("f")
                .long("file")
                .takes_value(true)
                .help("Input file"),
        )
        .get_matches();
    match matches.value_of("file") {
        None => exit(0),
        Some(fname) => analyze(fname),
    }
}
